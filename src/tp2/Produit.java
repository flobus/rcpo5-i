package tp2;

public abstract class Produit {
	//Attributs
	protected String nom;
	protected float prix_achat; 
	protected float prix_vente;
	protected float quantite_initiale;
	protected float coffre_fort;
	//Constructeurs
	public Produit(String n, float pa, float pv, float qi) {
		nom = n;
		prix_achat = pa;
		prix_vente = pv;
		quantite_initiale = qi;
	}
	//M�thodes 
	public void sePresenter() {
		System.out.println(" ");
		System.out.println("Le produit " + " " + nom + " " +  "se vend a : " + " " + prix_vente + "euros " + " il en reste " + " " + quantite_initiale + " " + "en stock");

	}
	 public abstract void soldeMarchandise(int s);
	
	//Accesseurs
}
