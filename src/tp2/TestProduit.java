package tp2;

public class TestProduit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Electromenager e = new Electromenager("TÚlÚvision", 350, 250, 4, 1112); 
		e.sePresenter();
		e.augmenterStock(2);
		e.vendreMarchandise(2);
		e.soldeMarchandise(20);
		
		
		Fruits_vrac f = new Fruits_vrac("Banane", 1, 2, 2000, "Afrique");
		f.sePresenter();
		f.diminiuerStock(1500);
		f.vendreMarchandise(250);
		f.soldeMarchandise(15);
		
		Fruits_piece fp = new Fruits_piece("Ananas", 1, 2, 500,"Afrique");
		fp.sePresenter();
		fp.vendreMarchandise(501);
		fp.soldeMarchandise(85);
	}

}
