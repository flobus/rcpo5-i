package tp2;

public class Fruits_piece extends Fruit{

	public Fruits_piece(String n, float pa, float pv, float qi, String po) {
		super(n, pa, pv, qi, po);
		// TODO Auto-generated constructor stub
	}
	
	
	public void augmenterStock(int q) {
		System.out.println(" ");
		System.out.println("Le stock de " + " " + nom + "est augment� de :" + " " + q + " " + "piece(s)");
		quantite_initiale = quantite_initiale + q;
		sePresenter();	//fait appel directement � une autre m�thode afin d'all�ger le Test 
	}
	
	public void diminuerStock(int q) {
		System.out.println(" ");
		System.out.println("Le stock de " + " " + nom + "est diminu� de :" + " " + q + " " + "pi�ce(s)");
		quantite_initiale = quantite_initiale - q;
		sePresenter();	//fait appel directement � une autre m�thode afin d'all�ger le Test
	}
	
	public void vendreMarchandise(int x) {
		if (x <= quantite_initiale) {
			System.out.println("Un consommateur a achet�" + " " + x + " " + "pi�ce(s) de" + " " + nom );
			quantite_initiale = quantite_initiale - x;
			coffre_fort += prix_vente;
			sePresenter();
		}
		else {
			System.out.println("Vous ne pouvez pas vendre plus de" + " " + stockFruits + " " + " piece(s) de" + " " + nom);
		}
	}
	
	public void soldeMarchandise(int ms) {
		System.out.println(" ");
		System.out.println("Les soldes vous permettent de r�duire le prix de" + " " + prix_vente + "euros" + " " + "de " + " " + ms + "%");
		float solde = prix_vente * (ms/100);
		prix_vente = prix_vente - solde;
		sePresenter();
	}
}
