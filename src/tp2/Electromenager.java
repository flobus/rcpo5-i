package tp2;

public class Electromenager extends Produit {
	//Attributs 
	protected int nb_colis;
	protected int code_barre;
	//Constructeur
	public Electromenager(String n, float pa , float pv, float qi, int cb) {
		super( n,  pa,  pv,  qi );
		code_barre = cb;
	}
	//M�thodes
	public void augmenterStock(float q) {
		System.out.println(" ");
		System.out.println("Le nombre de colis de" + " " + nom + " " + "est augment� de :" + " " + q + " ");
		quantite_initiale = quantite_initiale + q;
		sePresenter();	//fait appel directement � une autre m�thode afin d'all�ger le Test 
	}
	
	public void diminuerStock(float q) {
		System.out.println(" ");
		System.out.println("Le nombre de colis " + " " + nom + "est diminu� de :" + " " + q + " ");
		quantite_initiale = quantite_initiale - q;
		sePresenter();	//fait appel directement � une autre m�thode afin d'all�ger le Test 
	}
	
	
	public void vendreMarchandise(int v) {
		if (v <= quantite_initiale) {
			System.out.println("Un consommateur a achet�" + " " + v + " " + nom );
			quantite_initiale = quantite_initiale - v;
			coffre_fort += prix_vente;
			sePresenter();
		}
		else {
			System.out.println("veuillez respecter la limite des stocks disponibles.");
		}
	}
	
	public void soldeMarchandise(int sol) {
		System.out.println(" ");
		System.out.println("Les soldes vous permettent de r�duire le prix de" + " " + prix_vente + "euros" + " " + "de " + " " + sol + "%");
		float solde = prix_vente * (sol/100);
		prix_vente = prix_vente - solde;
		sePresenter();
	}
}