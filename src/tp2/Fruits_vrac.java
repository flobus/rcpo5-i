package tp2;

public class Fruits_vrac extends Fruit {

	public Fruits_vrac(String n, float pa, float pv, float qi, String po) {
		super(n, pa, pv, qi, po);
		// TODO Auto-generated constructor stub
	}

		//M�thodes
		public void augmenterStock(float q) {
			System.out.println(" ");
			System.out.println("Le stock de " + " " + nom + "est augment� de :" + " " + q + " " + "kg");
			stockFruits = stockFruits + q;
			sePresenter();	//fait appel directement � une autre m�thode afin d'all�ger le Test 
		}
		
		public void diminiuerStock(float q) {
			System.out.println(" ");
			System.out.println("Le stock de " + " " + nom + " " + "est diminiu� de :" + " " + q + " " + "kg");
			stockFruits = stockFruits - q;
			sePresenter();	//fait appel directement � une autre m�thode afin d'all�ger le Test 
		}
		
		public void vendreMarchandise(int w) {
			if (w <= quantite_initiale) {
				System.out.println("Un consommateur a achet�" + " " + w + " " + "kg de" + " " + nom );
				stockFruits = stockFruits - w;
				coffre_fort += prix_vente;
				sePresenter();
			}
			else {
				System.out.println("Vous ne pouvez pas vendre plus de" + stockFruits + " " + " kg de" + " " + nom);
			}
		}
		public void soldeMarchandise(int sm) {
			System.out.println(" ");
			System.out.println("Les soldes vous permettent de r�duire le prix de" + " " + prix_vente + "euros" + " " + "de " + " " + sm + "%");
			float solde = prix_vente * (sm/100);
			prix_vente = prix_vente - solde;
			sePresenter();
			
		}
}
